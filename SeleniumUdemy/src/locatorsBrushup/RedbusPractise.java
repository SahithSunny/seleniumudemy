package locatorsBrushup;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RedbusPractise {
	
	public static void SelectSeats(WebDriver driver) throws InterruptedException
	{
		//select the seats
	    driver.findElement(By.xpath("//div[@class='button view-seats fr']")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//span[@title='HYDERABAD']")).click();
	   // driver.findElement(By.xpath("//header[@class='modal-header clearfix']/div[2]/span")).click();
	    Thread.sleep(1000);
	    driver.findElement(By.xpath("//ul[contains(@class,'height-bpdp-single-deck')]/li[2]/div[1]/div")).click();
	}
	

	public static void main(String[] args) throws AWTException, InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://redbus.in/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
        //Give from & to destination details along with dates
	    driver.findElement(By.xpath("//input[@id='src']")).sendKeys("Secundrabad");
	    Robot r = new Robot();
	    r.keyPress(KeyEvent.VK_ENTER);
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//input[@id='dest']")).sendKeys("Mumbai");
	    r.keyPress(KeyEvent.VK_ENTER);
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//input[@id='onward_cal']/following-sibling::label")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//div[@id='rb-calendar_onward_cal']//button[text()='>']")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//div[@id='rb-calendar_onward_cal']/table/tbody/tr[3]/td[7]")).click();
	    driver.findElement(By.xpath("//label[text()='Return Date']")).click();
	    Thread.sleep(2000);
	    
	    driver.findElement(By.xpath("//div[@id='rb-calendar_return_cal']/table/tbody/tr[4]/td[5]")).click();
	    driver.findElement(By.xpath("//button[@id='search_btn']")).click();
	    driver.findElement(By.xpath("//div[@class='alt-msg m-top-16']/following-sibling::ul/li[2]/div/div[4]/div")).click();
	    
	    SelectSeats(driver);
	    
	    driver.findElement(By.xpath("//button[@id='gotoseat_btn']")).click();	
	    driver.findElement(By.xpath("//div[@class='lower-canvas canvas-wrapper']/canvas")).click();
	    Thread.sleep(5000);
        driver.close();
	    
	    
	    
	    
	}

}	
	


