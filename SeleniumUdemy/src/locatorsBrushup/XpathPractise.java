package locatorsBrushup;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class XpathPractise {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		driver.get("http://www.qaclickacademy.com/");
		driver.manage().window().maximize();
		
		//closed the model popup, used if else because, the model sometimes popping & sometimes not
		WebElement element = driver.findElement(By.xpath("//button[text()='NO THANKS']"));
		if(element.isDisplayed() && element.isEnabled()) {
			element.click();
		}
		
	    driver.findElement(By.xpath("//li[@class='active']/following-sibling::li[3]/a")).click();
	    driver.findElement(By.xpath("//li[@id='tablist1-tab1']")).click();
	    //traversing from child to an another child
	    driver.findElement(By.xpath("//li[@id='tablist1-tab1']/following-sibling::li[1]")).click();
		driver.findElement(By.xpath("//li[@id='tablist1-tab1']/following-sibling::li[2]")).click();
		driver.findElement(By.xpath("//li[@id='tablist1-tab1']/following-sibling::li[3]")).click();
		
		Thread.sleep(3000);
		
		driver.close();
	}

}
