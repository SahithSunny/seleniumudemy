package locatorsBrushup;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class EngagebayLogin {
	

	
	private static WebDriver driver = null;

	public WebDriver getDriver() throws InterruptedException{
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		if(driver == null){
			driver = new ChromeDriver();
		
			driver.get("https://app.engagebay.com");
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			driver.manage().window().maximize();
		
			//switching to Iframe to close the chat popup
			driver.switchTo().frame(0);
			
			Actions act = new Actions(driver); 
			WebElement ele = driver.findElement(By.xpath("//body/div[@id='app']/div"));
			act.moveToElement(ele).build().perform();
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("//div[@class='close_chat_bubble']/img")).click();
			driver.switchTo().defaultContent();
		}
		return driver;
		
	}
	
	    public void eblogin () {
		
		driver.findElement(By.xpath("//input[@placeholder='Work Email']")).sendKeys("sahith@engagebay.com");
		driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("sahith007");
		driver.findElement(By.xpath("//button[text()='LogIn']")).click();
		
	}
	
	
	public static void main(String args[]) throws InterruptedException {
		
		EngagebayLogin eblog = new EngagebayLogin();
		eblog.getDriver();
		eblog.eblogin();
		
		
	}
	
	
}
