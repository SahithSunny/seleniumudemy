package locatorsBrushup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class facebook {

	public static void main(String[] args) throws InterruptedException {
     
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://facebook.com");
        driver.manage().window().maximize();

        
        //find element using css selector
        // syntax for xpath //Tagname[@attribute='value']
        
        /* driver.findElement(By.cssSelector("#username")).sendKeys("admin");
        driver.findElement(By.cssSelector("#password")).sendKeys("123456");
        driver.findElement(By.cssSelector("#Login")).click();  */
        
        // custom syntax for css - tagname[attribute='value']
        
        driver.findElement(By.cssSelector("input[id='email']")).sendKeys("cssselector");
        driver.findElement(By.cssSelector("input[name='pass']")).sendKeys("password");
        driver.findElement(By.cssSelector("input[value='Log In']")).click();
        Thread.sleep(2000);
        System.out.println(driver.findElement(By.xpath("//div[@role='alert']")).getText());
         
        Thread.sleep(2000);
        driver.close();
	}

}
