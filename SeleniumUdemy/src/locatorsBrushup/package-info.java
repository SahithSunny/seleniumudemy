/*
 In this package w'll look into all locators & how to use each type of locators in all browsers
 
 1. Importance of locators in selenium
 2. Locators supported by selenium webdriver
 -> ID
 -> ClassName
 -> Name
 -> Xpath
 -> Css
 -> LinkText
 
 3. How to get locators from FireFox browser.
 4. How to get locators from Chrome browser.
 5. How to validate Xpath & Css from the browser
 6. Customizes Xpath/Css Generation techniques.
 -> Standard pattern
 -> Tag traverse
 -> Regular expressions
 

 
  
 */
/**
 * @author sunny
 *
 */
package locatorsBrushup;