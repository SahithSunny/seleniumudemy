package locatorsBrushup;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class RediffPractise {

	public static void main(String[] args) throws InterruptedException {
     
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://rediff.com");
        driver.manage().window().maximize();

        driver.findElement(By.cssSelector("a[title*='Sign in']")).click();
        driver.findElement(By.xpath("//input[@id='login1']")).sendKeys("helloadmin");
        driver.findElement(By.xpath("//input[@name='passwd']")).sendKeys("password");
        driver.findElement(By.xpath("//input[@value='Go']")).submit();
        Thread.sleep(2000);
        driver.close();
	}

}
