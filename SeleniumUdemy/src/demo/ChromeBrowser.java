package demo;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class ChromeBrowser {
	

	public static void main(String[] args) throws InterruptedException {
		
        /* create driver object for chrome browser
		 above driver object should strictly implement all the methods of WebDriver Interface.
		
		Before invoking the browser, should specify the browser key & path of path of browser exe >> Then should invoke the browser  */ 
		
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		
		//Specify the browser URL to open in chrome browser
		driver.get("https://app.engagebay.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS); // Implicit wait
		// get page title
		// System.out.println(driver.getTitle());
		// validate whether redirected to exact page/Not
		System.out.println(driver.getCurrentUrl());
		// Get the current URL's page source
		// System.out.println(driver.getPageSource());
		
		// switching into iframe to close the chat
		Thread.sleep(2000);
		int size = driver.findElements(By.tagName("iframe")).size();
		System.out.println(size);
		driver.switchTo().frame(0);
		driver.findElement(By.xpath("//button[@class='chat__prompt-close']/img")).click();
		// mouse hover on the chat element
		Actions action = new Actions(driver);
		WebElement ele = driver.findElement(By.xpath("//body/div[@id='app']/div"));
		action.moveToElement(ele).build().perform();
		//click o the close icon
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@class='close_chat_bubble']/img")).click();
		Thread.sleep(2000);
		// switch back to parent page
		driver.switchTo().defaultContent();
		
		
		driver.findElement(By.linkText("Sign Up")).click(); // click on
		// navigation commands in selenium 
		driver.navigate().back();
		String title = driver.getTitle();
		System.out.println("Title of login page is "+title);
		Thread.sleep(3000);
		
		driver.navigate().forward();
		String SignupTitle =driver.getTitle();
		System.out.println("Title of SignUp page is ("+SignupTitle+")");
		Thread.sleep(3000);
		
		driver.close();
	
	}

}
