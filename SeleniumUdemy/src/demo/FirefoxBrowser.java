package demo;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxBrowser {

	public static void main(String args[]) throws InterruptedException {
		
		
		System.setProperty("webdriver.gecko.driver", "D:\\Libs\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
		driver.navigate().to("https://app.engagebay.com");  //OPen the browser in firefox
		driver.manage().window().maximize();  // maximizing the window
		
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS); // Implicit wait
		
		
		System.out.println(driver.getTitle());
		
		driver.close();
		
		
		
	}

	
	
}
