package selenium_Grid;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/*DesiredCapabilities: Incase we are running the TC's remotely in any server, have to use this DC class
-> Have to create DC class & mention the browser & operating system we are running the scripts. 
* */

public class Selenium_Grid_example {

	public static void main(String[] args) throws MalformedURLException {
		
		DesiredCapabilities dc= new DesiredCapabilities();
		dc.setBrowserName("chrome");
		dc.setPlatform(Platform.WINDOWS);
		
		WebDriver driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),dc);
		driver.get("https://engagebay.com");
		driver.manage().window().maximize();
		

	}

}
