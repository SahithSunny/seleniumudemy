package syncronization;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class FluentWaitDemo1 {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("https://the-internet.herokuapp.com/dynamic_loading");
		
		driver.findElement(By.xpath("//a[contains(text(),'Example 1:')]")).click();
		driver.findElement(By.xpath("//button[contains(text(),'Start')]")).click();
		
		//Implementing FluentWait condition & customized method
		
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofSeconds(4)).ignoring(NoSuchElementException.class);
		
		
		WebElement foo = wait.until(new Function<WebDriver, WebElement>()
		{
			//Defining a customized method with return type of WebElement
			public WebElement apply(WebDriver driver)
			{
				 WebElement displayText = driver.findElement(By.xpath("//div[@id='finish']/h4"));
				if(displayText.isDisplayed())
				{
					return displayText;
				}
				else
					return null;
				
			}
		});
		
		System.out.println(driver.findElement(By.xpath("//div[@id='finish']/h4")).isDisplayed());
		System.out.println(driver.findElement(By.xpath("//div[@id='finish']/h4")).getText());
		
				
	}

}
