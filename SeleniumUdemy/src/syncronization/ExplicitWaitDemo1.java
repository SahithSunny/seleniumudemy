package syncronization;

import java.util.Arrays;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWaitDemo1 {

	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		driver.manage().window().maximize();
		//driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		String[] groceries = {"Tomato","Carrot","Beans"};
		List<WebElement>  allItems= driver.findElements(By.xpath("//h4[@class='product-name']"));
		
		// While calling the method in main also we should use parameters
		addToCart(driver, groceries, allItems);
		Purchase(driver);

	}

	
	
	
	//Declaring the List variables in main & passing to the method using method parameters
	public static void addToCart(WebDriver driver, String[] groceries, List<WebElement> allItems )
	{
		int k=0;
		
		for(int i=0; i<allItems.size(); i++)
		{
			// Format all the items present by removing - & extra spaces
			String[] allItems1 = allItems.get(i).getText().split("-");
			String formatedAllItems = allItems1[0].toString().trim();
			
			// To use contains method on groceries- should convert it from array to list
			List<String> groceriesList = Arrays.asList(groceries);
			
			if(groceriesList.contains(formatedAllItems))
			{
				driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
				k++;
				
				if(k==groceries.length)
					break;
	
			}
			
		}
	}
	
	
	
	
	public static void Purchase(WebDriver driver)
	{
		driver.findElement(By.xpath("//img[@alt='Cart']")).click();
		driver.findElement(By.xpath("//button[text()='PROCEED TO CHECKOUT']")).click();
		
		// Explicit wait- 1.Declare WebDriverWait 2.specify the condition using webdriverwait object
		//WebDriverWait w = new WebDriverWait(driver, 5);
		//w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@class='promoCode']")));
		
		//Applying promocode & checkout
		driver.findElement(By.xpath("//input[@class='promoCode']")).sendKeys("rahulshettyacademy");
		driver.findElement(By.xpath("//button[@class='promoBtn']")).click();
		
		//w.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='promoInfo']")));
		System.out.println(driver.findElement(By.xpath("//span[@class='promoInfo']")).getText());
		
		driver.findElement(By.xpath("//button[text()='Place Order']")).click();
		
		new Select(driver.findElement(By.xpath("//div[@class='wrapperTwo']//div//select"))).selectByVisibleText("India");
		driver.findElement(By.xpath("//input[@class='chkAgree']")).click();
		driver.findElement(By.xpath("//button[text()='Proceed']")).click();
	    
		System.out.println(driver.findElement(By.xpath("//span[contains(text(),'Thank you, your order has been placed successfully')]")).getText());
		
		
	}
	
}
