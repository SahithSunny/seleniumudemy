package syncronization;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class ImplicitWaitsDemo1 {

	public static void AddtoCart(WebDriver driver)
	{
		driver.findElement(By.xpath("//*[text()='Brocolli - 1 Kg']/following-sibling::div[2]/button")).click();
		driver.findElement(By.xpath("//*[text()='Cauliflower - 1 Kg']/following-sibling::div[2]/button")).click();
		driver.findElement(By.xpath("//*[text()='Cucumber - 1 Kg']/following-sibling::div[2]/button")).click();
	}
	
	public static void checkout(WebDriver driver)
	{
		driver.findElement(By.cssSelector("a.cart-icon")).click();
		driver.findElement(By.xpath("//button[text()='PROCEED TO CHECKOUT']")).click();
		//applying promocode while checkout
		driver.findElement(By.cssSelector("input.promoCode")).sendKeys("rahulshettyacademy");
		driver.findElement(By.cssSelector("button.promoBtn")).click();
		String actual = driver.findElement(By.cssSelector("span.promoInfo")).getText();
		
		Assert.assertEquals(actual, "Code applied ..!");
		
		driver.close();
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		AddtoCart(driver);
		checkout(driver);

	}

}
