package syncronization;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ExplicitWaitExercise1 {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("http://www.itgeared.com/demo/1506-ajax-loading.html");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//h2[text()='Demo']/following-sibling::a[1]")).click();
		
		//WebDriverWait wt = new WebDriverWait(driver, 6);
		//wt.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='results']")));
		
		WebElement text= driver.findElement(By.xpath("//div[@id='results']"));
		System.out.println( text.getText() );
		
		Assert.assertEquals(text, "Process completed! This response has been loaded via the Ajax request directly from the web server, without reoading this page.");
		

	}

}
