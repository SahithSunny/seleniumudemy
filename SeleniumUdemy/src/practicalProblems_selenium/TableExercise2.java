package practicalProblems_selenium;

/*From qaclickacademy- WebTable 
 * 1. Get the Table rows count
 * 2. Get the Table Column count
 * 3. Print the Data from the second row of the Table
*/

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableExercise2 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		int rowCount = driver.findElements(By.xpath("//table[@id='product']/tbody/tr")).size();
		System.out.println("roe count of Table is "+rowCount);
		
		int columnSize = driver.findElements(By.xpath("//table[@id='product']/tbody/tr/th")).size();
		System.out.println("Column size of Table is "+columnSize);
		
		//Printing the Second Row- each column values from the Table
		List<WebElement> row = driver.findElements(By.xpath("//table[@id='product']/tbody/tr[2]/td"));
		for(int i=0; i<row.size(); i++)
		{
			String rowvalue= driver.findElements(By.xpath("//table[@id='product']/tbody/tr[2]/td")).get(i).getText();
			System.out.println(rowvalue);
		}

	}

}
