package practicalProblems_selenium;

/*From ksrtc website, select autosuggestive dropdown from & enter 'BENG'
 * Verify the option selected have airport text init
 * if not try to hit down arrow key untill the specific value got selected*/

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AutosuggestiveDropdown {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://ksrtc.in/oprs-web/");
		driver.manage().window().maximize();
	
		
		driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys("BENG");
		Thread.sleep(4000);
		driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys(Keys.DOWN);
		driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys(Keys.DOWN);
		
		//System.out.println(driver.findElement(By.xpath("//input[@id='fromPlaceName']")).getText());
		
		/*In the above scenario we are unable to get the hidden field value, so we use 'JavascriptExecutor'method
		 to handle with the hidden elements in selenium. */
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String script= "return document.getElementById(\"fromPlaceName\").value;";
		String text = (String) js.executeScript(script);
		System.out.println(text);
		
		int i=0;
		while(!text.equalsIgnoreCase("BENGALURU AIRPORT"))
		{
			i++;
			Thread.sleep(1000);
			driver.findElement(By.xpath("//input[@id='fromPlaceName']")).sendKeys(Keys.DOWN);
			
		    text = (String) js.executeScript(script);
			System.out.println(text);
			
			//Incase the element has not found even after the 10 Key DOWN actions, then break the while loop
			if(i>10)
			{
				break;
			}
		
		}
		
		if(i>10)
		{
			System.out.println("Element not found");
		}
		else
		{
			System.out.println("Element fount & Test case Passed");
			Thread.sleep(4000);
			driver.quit();
		}
		
		
	
		


	}

}
