package practicalProblems_selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

/*In QAclickacademy -> from autosuggestive dropdown, enter any value hover on to the desired option
& verify that the desired element got selected in the dropdown.*/

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class AutosuggestiveExercise {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//input[@id='autocomplete']")).sendKeys("Uni");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='autocomplete']")).sendKeys(Keys.DOWN);
		System.out.println(driver.findElement(By.xpath("//input[@id='autocomplete']")).getText());
		
		// System.out.println(driver.findElement(By.xpath("//input[@id='autocomplete']")).getAttribute("value")); - can use this instead of JavascriptExecutor
		 
		// Handling Dropdown Hidden option Using JavascriptExecutor method
		JavascriptExecutor js = (JavascriptExecutor)driver;
		String script = "return document.getElementById(\"autocomplete\").value;";
		String text = (String) js.executeScript(script);
		System.out.println(text);
		
		int i=0;
		while(!text.contains("USA"))
		{
			i++;
			driver.findElement(By.xpath("//input[@id='autocomplete']")).sendKeys(Keys.DOWN);
			
	       if(i>10)
	         break;
	       
	       text = (String) js.executeScript(script);
		   System.out.println(text);
		}
		
		Assert.assertEquals(text, "United States (USA)");

	}

}
