package practicalProblems_selenium;

/*Goto Crickbuzz- land on any scoreboard webtable
 * Get all the score column values & sum those values
 * and compare the sum you got with the Total sum value in the Table
 */

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class TableExercise1 {

	public static void main(String[] args) {
		int sum=0;
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.cricbuzz.com/live-cricket-scorecard/22585/aus-vs-nz-1st-odi-new-zealand-tour-of-australia-2020");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement parentTable= driver.findElement(By.xpath("//div[@id='innings_1']/div[@class='cb-col cb-col-100 cb-ltst-wgt-hdr']"));
	
		int count = parentTable.findElements(By.xpath("//div[@class='cb-col cb-col-100 cb-scrd-itms']/div[3]")).size();
		
		for(int i=0; i<count-15; i++)
		{
			String score= parentTable.findElements(By.xpath("//div[@class='cb-col cb-col-100 cb-scrd-itms']/div[3]")).get(i).getText();
			//converting the string into int
			int scoreNumb = Integer.parseInt(score);
			sum=sum+scoreNumb;
			
		}
		//System.out.println(sum);
		
        // calculating the Extras into the sum
		String total =driver.findElement(By.xpath("//div[text()='Extras']/following-sibling::div")).getText();
		int TotalScore =Integer.parseInt(total);
		int summedScore = sum+TotalScore;
		System.out.println(summedScore);
		
		//Getting the Total from table & converting the string into Integer
		String actualScore = driver.findElement(By.xpath("//div[text()='Total']/following-sibling::div")).getText();
		int actualScoreValue = Integer.parseInt(actualScore);
		System.out.println(actualScoreValue);
		
		//Using Assert method comparing the summed total with ActualTotal
		Assert.assertEquals(summedScore, actualScoreValue);
		
	}

}
