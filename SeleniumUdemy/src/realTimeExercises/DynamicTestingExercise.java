package realTimeExercises;

/*Exercise to Dynamically perform the actions, without providing any values Hadcode - https://www.rahulshettyacademy.com/AutomationPractice/
 *1. SElect any checkbox & Grab the label name of the checkbox option 
 *2. Select an option from Dropdown, here the option should come from Step-1 (Store the checkbox variable in a element & Drive it all the way) 
 *3. Enter the step-2 grabbed text into the Alert editbox & hit on alert button
 *4. Verify that the grabbed text from above step is present in the alert text
 */

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class DynamicTestingExercise {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//1. Select any checkbox & Grab the label name of the checkbox option 
		driver.findElement(By.xpath("//div[@id='checkbox-example']//label[2]/input")).click();
	   String checkbox = driver.findElement(By.xpath("//div[@id='checkbox-example']//label[2]")).getText();
	   System.out.println(checkbox);
	   
	   //2. Select an option from Dropdown, here the option should come from Step-1
	  Select dropdown =  new Select(driver.findElement(By.xpath("//select[@id='dropdown-class-example']"))); 
	  dropdown.selectByVisibleText(checkbox);
	  
	  //3. Enter the step-2 grabbed text into the Alert editbox & hit on alert button
	  driver.findElement(By.xpath("//input[@id='name']")).sendKeys(checkbox);
	  driver.findElement(By.xpath("//input[@id='alertbtn']")).click();
	  
	  String alertText = driver.switchTo().alert().getText();
	  System.out.println(alertText);
	  driver.switchTo().alert().accept();
	  
	  //Assert.assertTrue(driver.switchTo().alert().getText().contains(checkbox)); - can use assert directly
	/*  if(alertText.contains(checkbox))  - can use if else directly on strings without spliting & looping
	  {
		  System.out.println("Yes!! The grabbed text is present in the Alert message text");
		  
	  }
	  else
	  {
		  System.out.println("Test case got failed");
	  }*/

	 
	  // 4. Verify that the grabbed text from above step is present in the alert text
	  // split the alert message with - space & check selected checkbox label is presend in the alert text string
	  
	  String[] alertWords = alertText.split(" ");
	  for (int i=0; i<alertWords.length; i++)
	  {
		 
		  if(alertText.contains(alertWords[i]))
		  {
			  System.out.println("Yes!! The grabbed text is present in the Alert message text");
			  break;
		  }
		  else
		  {
			  System.out.println("Test case got failed");
		  }
	  }
	  	

	}

}
