package realTimeExercises;

import java.util.Iterator;
import java.util.Set;

/*Exercise : 1. To print all the links count on this page. 
2. Get the count of links only in the footer section(we need to limit the driver to the footer section only
    by creating the new driver which  will be subset of actual driver).
3. Get the links count in 1st column of the footer section, out of the 4 columns
4. Click on the each link from above first column footer links to check all the links are working fine,
   & get the Title of the each page.

    */

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LimitDriverScope1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.rahulshettyacademy.com/AutomationPractice/");
		

        // Get the links count using Tagname 'a'
		System.out.println(driver.findElements(By.tagName("a")).size());
		
		//Get the links count only in the footer section only.
		WebElement footerdriver = driver.findElement(By.xpath("//div[@id='gf-BIG']"));
		System.out.println("Footer links count "+footerdriver.findElements(By.tagName("a")).size());

		//Get the links count for first column in the footer
		WebElement firstFooterColumn = driver.findElement(By.xpath("//table[@class='gf-t']/tbody/tr/td[1]/ul"));
		System.out.println(firstFooterColumn.findElements(By.tagName("a")).size());
		
		//Click on the each link from above WebElement & print the Title of each page.
		for(int i=1; i<firstFooterColumn.findElements(By.tagName("a")).size(); i++)
		{
			String controlClick = Keys.chord(Keys.CONTROL,Keys.ENTER);
			firstFooterColumn.findElements(By.tagName("a")).get(i).sendKeys(controlClick);
            Thread.sleep(5000);

		}
		
		//Getting the page titles of each page
		Set<String> windoHandles= driver.getWindowHandles();
		Iterator<String> it = windoHandles.iterator();
		
		while(it.hasNext())
		{
			
			driver.switchTo().window(it.next());
			System.out.println(driver.getTitle());
		}
	}

}
