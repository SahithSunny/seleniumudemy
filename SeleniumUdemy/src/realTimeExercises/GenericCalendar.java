package realTimeExercises;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GenericCalendar {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.path2usa.com/travel-companions");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//input[@id='travel_date']")).click();
		
		//Traverse to the November month
		while(!driver.findElement(By.cssSelector("[class='datepicker-days'] th[class='datepicker-switch']")).getText().contains("November"))
		{
			driver.findElement(By.cssSelector("[class='datepicker-days'] [class='next']")).click();
		}
		
		
		// November 22 - find the date from the picker Once Month got selected
		List<WebElement> dates = driver.findElements(By.xpath("//*[contains(@class,'day')]"));
		
		for(int i=0; i<dates.size(); i++)
		{
			String date = driver.findElements(By.xpath("//*[contains(@class,'day')]")).get(i).getText();
			
			if(date.equalsIgnoreCase("22"))
			{
				driver.findElements(By.xpath("//*[contains(@class,'day')]")).get(i).click();
				break;
			}
			
		}
	}

}
