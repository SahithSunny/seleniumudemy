package ajax_Windowhandling_Iframes_ActionsClass;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandleExercise1 {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//a[text()='Multiple Windows']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Click')]")).click();
		
		//Handling multiple windows- grtWindowHandles
		Set<String> windows = driver.getWindowHandles();
		
		Iterator<String> it = windows.iterator();
        String parentID = it.next();
        String childID = it.next();
        
        driver.switchTo().window(childID);
        System.out.println(driver.findElement(By.xpath("//div[@class='example']/h3")).getText());
        
        //switching back to parent window
        driver.switchTo().window(parentID);
        System.out.println(driver.findElement(By.xpath("//div[@class='example']/h3")).getText());
	}

}
