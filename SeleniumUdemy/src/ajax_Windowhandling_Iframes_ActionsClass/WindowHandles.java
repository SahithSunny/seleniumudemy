package ajax_Windowhandling_Iframes_ActionsClass;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandles {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("https://accounts.google.com/signup");
		driver.findElement(By.xpath("//a[text()='Help']")).click();
		System.out.println(driver.getTitle());
		
		// Get all the windows using getWindowHandles
		Set<String> windowHandles= driver.getWindowHandles();
		
		// To get the Id's of each window have to loop the 'windowHandles' set using a iterator.
		Iterator<String> it = windowHandles.iterator();
		String parentId= it.next();
		String childId = it.next();
		
		//Switch to the child window by passing the child window id i.e, 'childId'
		driver.switchTo().window(childId);
		System.out.println(driver.getTitle());
		// working in a child window
		driver.findElement(By.xpath("//form[@id='search-form']/input[1]")).sendKeys("change password");
		driver.findElement(By.xpath("//form[@id='search-form']/input[1]")).sendKeys(Keys.ENTER);
		// navigate back to the parent window.
		//driver.switchTo().defaultContent(); 
		driver.switchTo().window(parentId);
		System.out.println("Title after switching to parent tab");
		System.out.println(driver.getTitle());

	}

}
