package ajax_Windowhandling_Iframes_ActionsClass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NestedFramesExercise {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com");
		driver.manage().window().maximize();
		
		driver.findElement(By.linkText("Nested Frames")).click();
		
		System.out.println(driver.findElements(By.tagName("frame")).size());
		
		driver.switchTo().frame(0);
		//driver.switchTo().frame("frame-top") - parent frame, using parent frame name
		System.out.println(driver.findElements(By.tagName("frame")).size());
		driver.switchTo().frame(driver.findElement(By.xpath("//frame[@name='frame-middle']")));
		//driver.switchTo().frame("frame-middle"); - child frame, using child frame name
		System.out.println(driver.findElement(By.xpath("//div[@id='content']")).getText());
		
		driver.switchTo().defaultContent();
		

	}

}
