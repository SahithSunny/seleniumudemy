package ajax_Windowhandling_Iframes_ActionsClass;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

public class IFramesDemo_DragDrop {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://jqueryui.com/droppable/");
		driver.manage().window().maximize();
		
		//Switch to Iframe to perform actions inside a Frame
		System.out.println(driver.findElements(By.tagName("iframe")).size()); // to get count of all the frames in browser
		//driver.switchTo().frame(0);-  switching to Frame Using ID
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']"))); // switching to frome using Frame webelement
		driver.findElement(By.xpath("//div[@id='draggable']")).click();
		
		//Drag & Drop element
		WebElement drag = driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement drop = driver.findElement(By.xpath("//div[@id='droppable']"));
	     
		Actions a = new Actions(driver);
		a.dragAndDrop(drag, drop).build().perform();
		
		//Verify weather Drag & Drop is performed/Not
		String expected = driver.findElement(By.xpath("//div[@id='droppable']/p")).getText();
		Assert.assertEquals("Dropped!", expected);
		
		driver.switchTo().defaultContent(); //***To switch out of Iframes***
		
	}

}
