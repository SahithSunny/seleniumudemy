package ajax_Windowhandling_Iframes_ActionsClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

/*Actions: is a class in selenium, by which we can handle below tasks
To mouseover on object in selenium?
Performing mouse & keyboard interactions
context click on element (Right Click on an element)
Double click on element
Drag & Drop an element*/

public class ActionsDemo1 {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.get("https://amazon.com/");
		
		Actions a = new Actions(driver);
		//move on to element
		WebElement move = driver.findElement(By.xpath("//a[@id='nav-link-accountList']"));
		a.moveToElement(move).build().perform();
		Thread.sleep(3000);
		
		//search item in capital letters in amazon & double click on the text
		a.moveToElement(driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']"))).click().keyDown(Keys.SHIFT)
		.sendKeys("Kindle").doubleClick().build().perform();
	
		Thread.sleep(3000);
		//Keydown & hit on enter
		for(int i=0; i<5; i++)
		{
			driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']")).sendKeys(Keys.ARROW_DOWN);
		}
		
		Thread.sleep(3000);
		driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']")).sendKeys(Keys.ENTER);
		
		
		
	}

}
