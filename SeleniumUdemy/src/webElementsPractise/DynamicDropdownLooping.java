package webElementsPractise;

import java.awt.AWTException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DynamicDropdownLooping {

	public static void main(String[] args) throws InterruptedException, AWTException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("https://www.expedia.co.in/");
	    driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[@id='tab-flight-tab-hp']")).click();
		WebElement from = driver.findElement(By.xpath("//input[@id='flight-origin-hp-flight']"));
		from.sendKeys("mum");
		Thread.sleep(2000);
		from.sendKeys(Keys.ARROW_DOWN);
		from.sendKeys(Keys.ENTER);
		
		WebElement to = driver.findElement(By.xpath("//input[@id='flight-destination-hp-flight']"));
		to.click();
		to.sendKeys("ch");
		Thread.sleep(2000);
		
		for(int i=1;i<5;i++) {
			to.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(1000);
		}
		
		to.sendKeys(Keys.ENTER);
		
		driver.findElement(By.xpath("//input[@id='flight-departing-hp-flight']")).click();
		driver.findElement(By.xpath("//tbody/tr[1]/td[7]/button[@data-day='5']")).click();
		
		driver.findElement(By.xpath("//input[@id='flight-returning-hp-flight']")).click();
		driver.findElement(By.xpath("//tbody/tr[2]/td[5]/button[@data-day='10']")).click();
		
		driver.findElement(By.xpath("//div[@class='cols-nested']/div[4]/div/div/ul/li/button")).click();
		WebElement adult = driver.findElement(By.xpath("//div[contains(@class,'traveler-selector-sinlge-room-data')]/div/div[4]/button"));
		
		for(int j=1;j<3;j++) {
			adult.click();
			Thread.sleep(2000);
			}
		
		WebElement children = driver.findElement(By.xpath("//div[contains(@class,'traveler-selector-sinlge-room-data')]/div[2]/div/div[4]/button"));
		
		for(int k=1;k<4;k++) {
		children.click();
		Thread.sleep(2000);
		}
		
		Select child1= new Select(driver.findElement(By.cssSelector("select[id='flight-age-select-1-hp-flight']")));
		child1.selectByVisibleText("2");
	
		Select child2 = new Select(driver.findElement(By.cssSelector("select[id='flight-age-select-2-hp-flight']")));
		child2.selectByValue("2");
		
		Select child3 = new Select(driver.findElement(By.cssSelector("select[id='flight-age-select-3-hp-flight']")));
		child3.selectByValue("2");
	    
		driver.findElement(By.xpath("//div[@class='cols-nested']/div[4]/div/div/ul/li/button")).click();
		driver.findElement(By.xpath("(//button[@class='btn-primary btn-action gcw-submit'])[1]")).click();
		
	}

}
