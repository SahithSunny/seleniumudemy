package webElementsPractise;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Section8Assigment1 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("https://rahulshettyacademy.com/AutomationPractice/");
		driver.manage().window().maximize();

		WebElement check = driver.findElement(By.xpath("//label/input[@id='checkBoxOption1']"));
		check.click();
		System.out.println("After checked the element- " + check.isSelected());
		Thread.sleep(3000);

		check.click();
		System.out.println("After unchecked the element- " + check.isSelected());
		Thread.sleep(3000);

		// Getting the count of checkboxes
		List<WebElement> checkbox = driver.findElements(By.xpath("//input[@type='checkbox']"));
		System.out.println("Cheboxes count is " + checkbox.size());

	}

}
