package webElementsPractise;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class IsEabledPractise {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
        driver.get("https://spicejet.com");
        
     /*   //before selecting the round trip option
        System.out.println("one way "+driver.findElement(By.xpath("//div[@id='Div1']")).isEnabled());
        driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_1']")).click();
        //after selecting the round trip option
        System.out.println("round trip "+driver.findElement(By.xpath("//div[@id='Div1']")).isEnabled());

       */
        
        System.out.println("disabled "+driver.findElement(By.xpath("//div[@id='Div1']")).getAttribute("style"));
        //select the round trip option
        driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_1']")).click();
        System.out.println("enabled "+driver.findElement(By.xpath("//div[@id='Div1']")).getAttribute("style"));
        Thread.sleep(2000);
        //again select One way option to fail the test deliberately
       // driver.findElement(By.xpath("//input[@id='ctl00_mainContent_rbtnl_Trip_0']")).click();
        if(driver.findElement(By.xpath("//div[@id='Div1']")).getAttribute("style").contains("1")) {
        	
        	System.out.println("Test pass");
        	Assert.assertTrue(true);
        }
        else {
        	
        	Assert.assertTrue(false);
        }
	}

}
