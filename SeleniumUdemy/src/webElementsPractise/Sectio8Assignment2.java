package webElementsPractise;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Sectio8Assignment2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("https://www.cleartrip.com/flights");
		driver.manage().window().maximize();
		
		WebElement from = driver.findElement(By.xpath("//input[@id='FromTag']"));
		from.click();
		from.sendKeys("ban");
		Thread.sleep(4000);
		from.sendKeys(Keys.ARROW_DOWN);
		from.sendKeys(Keys.ENTER);
		
		WebElement to = driver.findElement(By.xpath("//input[@id='ToTag']"));
		to.sendKeys("che");
		Thread.sleep(4000);
		to.sendKeys(Keys.ENTER);
		
		//Handling calendar 
		driver.findElement(By.xpath("//a[@class='ui-state-default ui-state-highlight ui-state-active ']")).click();
		
		//Handling dropdowns using select class
		Select adults = new Select(driver.findElement(By.xpath("//select[@id='Adults']")));
		adults.selectByVisibleText("3");
		
		Select children = new Select(driver.findElement(By.xpath("//select[@id='Childrens']")));
		children.selectByVisibleText("3");
		
		driver.findElement(By.xpath("//i[@class='rArrow blue']")).click();
		Select flightclass = new Select(driver.findElement(By.xpath("//select[@id='Class']")));
		flightclass.selectByIndex(1);
		driver.findElement(By.xpath("//input[@id='AirlineAutocomplete']")).sendKeys("SriLankan Airlines");
		//click on search button
		driver.findElement(By.xpath("//input[@id='SearchBtn']")).click();
		
		Thread.sleep(2000);
		driver.close();
		

	}

}
