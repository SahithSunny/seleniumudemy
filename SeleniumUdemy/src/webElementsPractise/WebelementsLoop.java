package webElementsPractise;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebelementsLoop {

	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException

	{

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");

		driver = new ChromeDriver();

		driver.get("http://www.spicejet.com/");

		driver.manage().window().maximize();

		System.out.println(driver.getTitle());

		//System.out.println(driver.findElement(By.cssSelector("input[id*='friendsandfamily']")).isSelected());

		//driver.findElement(By.cssSelector("input[id*='friendsandfamily']")).click();

		//System.out.println(driver.findElement(By.cssSelector("input[id*='friendsandfamily']")).isSelected());

		Thread.sleep(4000);

		List<WebElement> checkboxes = driver.findElements(By.xpath("//*[@id='discount-checkbox']/div"));

		int count = checkboxes.size();

		System.out.println(count);

		for (int i=0; i<count; i++)

		{

			String text = driver.findElements(By.xpath("//*[@id='discount-checkbox']/div")).get(i).getText();

			System.out.println("text is "+text);

			if(text.equalsIgnoreCase("Family and Friends")) {

				driver.findElements(By.xpath("//*[@id='discount-checkbox']/div")).get(i).click();

				System.out.println(driver.findElement(By.cssSelector("input[id*='friendsandfamily']")).isSelected());

				break;

			}

		}

	}
}