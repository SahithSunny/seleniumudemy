package webElementsPractise;

import java.awt.AWTException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class SpiceJetPractise {
	
	

	public static void main(String[] args) throws InterruptedException, AWTException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.spicejet.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//label[text()='Round Trip']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='ctl00_mainContent_ddl_originStation1_CTXT']")).sendKeys("hyd");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id='ctl00_mainContent_ddl_destinationStation1_CTXT']")).sendKeys("mum");
		driver.findElement(By.xpath("//span[@class='ui-icon ui-icon-circle-triangle-e']")).click();
		driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']/tbody/tr[2]/td[4]/a[text()='5']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@id='Div1']/button")).click();
		Thread.sleep(2000);
        driver.findElement(By.xpath("//table[@class='ui-datepicker-calendar']/tbody/tr[3]/td[2]/a[text()='10']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@class='paxinfo']")).click();        
        Select s = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Adult']")));
        s.selectByVisibleText("3");
 
        Select ss = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Child']")));
        ss.selectByVisibleText("3");
        
        Select sss = new Select(driver.findElement(By.xpath("//select[@id='ctl00_mainContent_ddl_Infant']")));
        sss.selectByVisibleText("3");
        Thread.sleep(2000);
        
        driver.findElement(By.xpath("//div[@class='paxinfo']")).click();    
        
        System.out.println(driver.findElement(By.xpath("//div[@id='divpaxinfo']")).getText());
        Assert.assertEquals(driver.findElement(By.xpath("//div[@id='divpaxinfo']")).getText(), "3 Adult, 3 Child, 3 Infant");
        //https://stackoverflow.com/questions/39735449/how-can-i-loop-through-list-of-webelements-and-select-one-webelement-with-a-cond?rq=1
        
        Assert.assertFalse(driver.findElement(By.xpath("//input[contains(@id,'StudentDiscount')]")).isSelected());
        driver.findElement(By.xpath("//input[contains(@id,'StudentDiscount')]")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//input[contains(@id,'StudentDiscount')]")).isSelected());
        
        System.out.println("Checkboxes count is "+driver.findElements(By.xpath("//input[@type='checkbox']")).size());

        
		
		Thread.sleep(5000);
		//driver.close();
		
	}

}
