package miscellaneousTopics;

import org.openqa.selenium.By;

/*"Delete Cookie check"
-> Log into any application, check that after deleting the session cookie, wheather its redirecting to logOut page
-> Can also delete all the Browse cookies before loading a web URL through selenium
*/

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;


public class DeleteCookiesDemo {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		//driver.manage().deleteAllCookies();
		driver.get("https://app.engagebay.com/login");
		
		//Login to EB application
		driver.findElement(By.xpath("//div[@class='form-group']/input[@type='email']")).sendKeys("sahith@engagebay.co");
		driver.findElement(By.xpath("//div[@class='form-group']/input[@type='password']")).sendKeys("sahith007");
		//Login Button
		driver.findElement(By.xpath("//button[text()='LogIn']")).click();
		String LoginTitle = driver.getTitle();
		
		Thread.sleep(10000);
		//Delete the Session cookie & check wheather redirecting to lodin page
		driver.manage().deleteCookieNamed("JSESSIONID");
		
	    // Verify wheather redirected to Login Page/Not
		Assert.assertEquals("EngageBay - Login", LoginTitle);
		
	
		
	}

}
