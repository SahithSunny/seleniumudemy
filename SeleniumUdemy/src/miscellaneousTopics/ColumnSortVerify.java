package miscellaneousTopics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

/*Verify wheather the table column values are sorted or not:
 -> Retrive the column values and push into a list 'actualList'
 -> Copy the actualList into another list 'copyList' and sort the 'copyList'
 -> compare the 'copyList' with the 'actualList' - if both are equal then, the actual list is in sorted order
 * */
	

public class ColumnSortVerify {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/offers");
		
		//click on the Table head to change the Sort Order
	//	driver.findElement(By.xpath("//thead/tr/th[2]")).click();
		driver.findElement(By.xpath("//thead/tr/th[2]")).click();
		
		//Declare an Empty ArrayList
		ArrayList<String> actualList = new ArrayList<String>();
		
		List<WebElement> firstList= driver.findElements(By.xpath("//tbody/tr/td[2]"));
		for(int i=0; i<firstList.size(); i++)
		{
			actualList.add(firstList.get(i).getText());
		}
		
		
		ArrayList<String> copyList = new ArrayList<String>();
		//Copy the actualList elements & place into copyList
		for(int i=0; i<actualList.size(); i++)
		{
			copyList.add(actualList.get(i));
		}
		
		//Sort the copyList (Ascending) & place it into sortList 
		Collections.sort(copyList);
		
		//To check the Descending sort of List
		Collections.reverse(copyList);
		
		System.out.println("------copyList------");
		for(String copyele: copyList)
		{
			System.out.println(copyele);
		}
		
		
		System.out.println("------actualList------");
		for(String actele: actualList)
		{
			System.out.println(actele);
		}
		
		
		//Compare the two Lists
		Assert.assertTrue(actualList.equals(copyList));
		
		
		

		
		
	}

}
