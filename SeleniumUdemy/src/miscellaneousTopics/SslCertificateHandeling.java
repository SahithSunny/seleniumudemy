package miscellaneousTopics;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/*Handling SSL certifates for Chrome Browser in Selenium
-> Ceate a Desired capabilities obj & add some capabilities(SSL certificate missing,insecure certificate...) to the general chrome profile
-> Combine the above DesiredCapabilities obj to the local browser using ChromeOPtions obj 
*/

public class SslCertificateHandeling {

	public static void main(String[] args) {
		
		//DesiredCapabilities obj & add capability type 
		//DesiredCapabilities dc = DesiredCapabilities.chrome();
		//dc.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
		//dc.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		//Add above DesiredCapability obj to the Local chrome browser using ChromeOptions obj
		ChromeOptions c = new ChromeOptions();
		//c.merge(dc);
		
		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver(c);
		driver.get("https://wrong.host.badssl.com/");
		
		
		
		

	}

}
