package eComWebsiteDemo;

import java.util.Arrays;

/* Incase we need to add the list of the items to add to a cart, then the previous Demo- class won't work.
   -> Firstly store the items in a array- then convet array into arrayList -we cannot directly store in list as list takes more memory than array.
   -> Format the string value got by split & trim methods for strings
   -> check whether the item in arraylist matches to the whole products list- if YES then click on add to cart button.
   
 */

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddToCartDemo2 {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String[] items = {"Carrot","Musk Melon","Strawberry"};
		int k=0;
		
		List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name")); 
		System.out.println(products.size());
		
		for(int i=0; i<products.size(); i++)
		{
			//formating the products by split & trim methods
			String[] name = products.get(i).getText().split("-");
			String productName= name[0].trim();
			
			//conveting array items into arrayList itemsList
			List<String> itemslist = Arrays.asList(items);
		
			if(itemslist.contains(productName))
			{
				driver.findElements(By.xpath("//div[@class='product-action']/button")).get(i).click();
				k++;
				
				if(k==items.length)
				{
					break;
				}
			}
		}
	}

}
