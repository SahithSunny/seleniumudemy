package eComWebsiteDemo;

/* Incase want to get the single cart item by looping all the cart elements*/

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AddToCartDemo {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "D:\\Libs\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.get("https://rahulshettyacademy.com/seleniumPractise/#/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		List<WebElement> products = driver.findElements(By.cssSelector("h4.product-name")); 
		System.out.println(products.size());
		
		for(int i=0; i<products.size(); i++)
		{
			String name = products.get(i).getText();
			
			if(name.contains("Pumpkin"))
			{
				System.out.println(name);
				// Navigate to that product's add to cart button & click 
				driver.findElements(By.xpath("//button[text()='ADD TO CART']")).get(i).click();
				break;
			}
		}
	}

}
